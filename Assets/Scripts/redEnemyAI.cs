﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class redEnemyAI : MonoBehaviour {

	public float health = 20;
	private float prevHealth = 20;
	private Rigidbody2D m_Rigidbody2D;
	public GameObject hitParticle;
	public GameObject deadParticle;
	public int damage = 5;
	private GameObject instantiatedObj;
	private bool isFacingLeft = true;
	private Animator m_Anim;   
	private float speed;
	private float playerDist;
	GameObject player;
	private float direction;

	public void Start(){
		prevHealth = health;
		transform.FindChild ("healthBar").localScale = new Vector3((health / 50), 0.08f, 1f);
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		m_Anim = GetComponent<Animator>();
		player = GameObject.Find ("Dreadman");
	}

	public void Update(){
		playerDist = Vector3.Distance (transform.position, player.transform.position);

		// take damage event

		if(prevHealth != health){
			AudioSource audio = GetComponent<AudioSource>();
			audio.Play();
			takeDamage();
			prevHealth = health;
		}

		speed = m_Rigidbody2D.velocity.x;
		m_Anim.SetFloat("speed", Mathf.Abs(speed));

		if (transform.position.x > 138) {
			Vector3 temp = transform.position;
			temp.x = -20;
			transform.position = temp;
		} else if(transform.position.x < -20){
			Vector3 temp = transform.position;
			temp.x = 138;
			transform.position = temp;
		}

		//move AI

		if (health > 0 && playerDist > 6 && playerDist < 10){

			if(m_Rigidbody2D.velocity.sqrMagnitude > 20){
				m_Rigidbody2D.velocity *= 0.99f;
			} else {
				if(isFacingLeft == true){
					m_Rigidbody2D.AddForce(new Vector2(-1f, 0) * 50);
				} else {
					m_Rigidbody2D.AddForce(new Vector2(1f, 0) * 50);
				}
			}

		} else if (health > 0 && playerDist <= 6){
			if(m_Rigidbody2D.velocity.sqrMagnitude > 10){
				m_Rigidbody2D.velocity *= 0.99f;
			} else {
				if(isFacingLeft){
					m_Rigidbody2D.AddForce(new Vector2(-1f, 0) * 25);
				} else {
					m_Rigidbody2D.AddForce(new Vector2(1f, 0) * 25);
				}
			}
		}

		//attack player

		if (health > 0 && playerDist < 2) {
			m_Anim.SetBool ("attack", true);
		} else {
			m_Anim.SetBool ("attack", false);
		}

		// send "hit" event to player

		if(m_Anim.GetBool("hit") == true){

			m_Anim.SetBool ("hit", false);
		}

		//change direction

		if (player.transform.position.x > transform.position.x && isFacingLeft) {
			isFacingLeft = false;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		} else if (player.transform.position.x < transform.position.x && !isFacingLeft) {
			isFacingLeft = true;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}

	}
	public void takeDamage(){
		transform.FindChild ("healthBar").localScale = new Vector3((health / 50), 0.08f, 1f);
		m_Anim.SetBool ("hit", true);
		instantiatedObj = (GameObject) Instantiate(hitParticle, transform.position, transform.rotation);
		Destroy(instantiatedObj, 3);
		if (isFacingLeft) {
			m_Rigidbody2D.AddForce (new Vector2 (2f, 3f) * 60);
		} else {
			m_Rigidbody2D.AddForce(new Vector2(-2f, 3f) * 60);
		}

		if (health <= 0) {
			transform.FindChild ("healthBar").localScale = new Vector3(0, 0.08f, 1f);
			player.GetComponent<customControls>().killCounter(1);
			instantiatedObj = (GameObject) Instantiate(deadParticle, transform.position, transform.rotation);
			Destroy (this.gameObject);
			Destroy(instantiatedObj, 3);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Player"){
			if (isFacingLeft == true ){
				direction = -20f;
			} else {
				direction = 20f;
			}
			
			player.GetComponent<customControls>().takeDamage(damage, direction);
		}
	}
}
