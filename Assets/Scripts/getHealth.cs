﻿using UnityEngine;
using System.Collections;

public class getHealth : MonoBehaviour {

	public GameObject healthAudio;
	private GameObject instantiatedObj;

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			int health = coll.gameObject.GetComponent<customControls>().health;
			health += 25;
			coll.gameObject.GetComponent<customControls>().health = health;
			Destroy (this.gameObject);

			instantiatedObj = (GameObject) Instantiate(healthAudio, transform.position, transform.rotation);
			if(health > 50){
				health = 50;
				coll.gameObject.GetComponent<customControls>().health = health;
			}
			Destroy(this.gameObject, 3);
		}
	}
}
