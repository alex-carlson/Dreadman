﻿using UnityEngine;
using System.Collections;

public class playAudio : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(Example());
	}

	IEnumerator Example() {
		AudioSource audio = GetComponent<AudioSource>();
		audio.Play();
		yield return new WaitForSeconds(0.1f);
		audio.Stop();
	}
}
