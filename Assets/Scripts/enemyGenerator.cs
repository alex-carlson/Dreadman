﻿using UnityEngine;
using System.Collections;

public class enemyGenerator : MonoBehaviour {

	public GameObject[] enemy;
	int enemiesSpawned;

	// Use this for initialization
	void spawnEnemy () {
		enemiesSpawned++;
		int randItem = Random.Range (0, 6);
		int longItem = Random.Range (0, enemy.Length);

		//spawn one thing
		Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);

		int kills = GameObject.Find ("Dreadman").GetComponent<customControls> ().kills;
		int activeEnemies;
		GameObject[] activeRedEnemies;
		GameObject[] activeBlueEnemies;

		activeRedEnemies = GameObject.FindGameObjectsWithTag ("RedEnemy");
		activeBlueEnemies = GameObject.FindGameObjectsWithTag ("Enemy");

		activeEnemies = activeRedEnemies.Length + activeBlueEnemies.Length;

		if(activeEnemies < 80 ){

			if(kills > 10){
				Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
				Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
				Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
				Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
				
				if (kills > 20) {
					//spawn 2 things
					Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
					Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
					Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
					Instantiate (enemy[randItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
					
					if(kills > 30){
						// spawn 4 things
						Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
						Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
						Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
						
						if(kills > 40){
							// spawn 12 things
							Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
							Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
							Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
							Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
							Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
							Instantiate (enemy[longItem], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
						}
					}
				}
			}
		}
	}
	
	// Update is called once per frame
	void Start() {

		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);
		Instantiate (enemy[Random.Range (0, 6)], new Vector2 (Random.Range(-16, 136), 0), transform.rotation);

		enemiesSpawned = 10;

		InvokeRepeating ("spawnEnemy", 10, 5.0f);
	}
}
