﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class customControls : MonoBehaviour {

	public GameObject hairParticle;
	private GameObject instantiatedObj;
	public int kills;
	private float damage = 2;
	public int health;
	private GameObject hair;
	public Sprite[] hairDamage;
	private Rigidbody2D m_Rigidbody2D;
	private Animator m_Anim;   
	public bool isAttacking = false;
	public GameObject hitUI;
	private bool fadeout = false;

	// Use this for initialization
	void Start () {
		kills = 0;
		health = 50;
		hair = GameObject.Find ("Head");
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		m_Anim = GetComponent<Animator>();
		GameObject.Find("highscore").GetComponent<Text>().text = "Best: "+PlayerPrefs.GetInt("High Score");
	}
	
	// Update is called once per frame
	void Update () {

		if (health >= 50) {
			hair.GetComponent<SpriteRenderer> ().sprite = hairDamage [0];
		} else if (health > 15 && health < 50) {
			hair.GetComponent<SpriteRenderer> ().sprite = hairDamage [1];
		} else if (health > 5 && health <= 15) {
			hair.GetComponent<SpriteRenderer> ().sprite = hairDamage [2];
		} else {
			hair.GetComponent<SpriteRenderer> ().sprite = hairDamage [3];
		}
//
//		if (transform.position.x > 138) {
//			Vector3 temp = transform.position;
//			temp.x = -20;
//			transform.position = temp;
//		} else if(transform.position.x < -20){
//			Vector3 temp = transform.position;
//			temp.x = 138;
//			transform.position = temp;
//		}

		if(fadeout == true && GameObject.Find("LevelAudio").GetComponent<AudioSource>().volume >= 0){
			GameObject.Find("LevelAudio").GetComponent<AudioSource>().volume -= Time.deltaTime;
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			gameover();
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Enemy") {
			other.GetComponent<EnemyAI>().health -= damage;
			//other.attachedRigidbody.AddForce(new Vector3( (transform.localScale.x * kills / 2 + 1), 1f, 0) * 75);
			instantiatedObj = (GameObject) Instantiate(hairParticle, transform.position, transform.rotation);
			Destroy(instantiatedObj, 3);
		} else if(other.tag == "RedEnemy") {
			other.GetComponent<redEnemyAI>().health -= damage;
			instantiatedObj = (GameObject) Instantiate(hairParticle, transform.position, transform.rotation);
			Destroy(instantiatedObj, 3);
		}
	}

	public void takeDamage(int hitPoints, float direction){
		m_Rigidbody2D.AddForce(new Vector2(direction, 2f) * 75);
		health -= hitPoints;

		hitUI.GetComponent<Animation> ().Play ();

		if (health < 0) {
			gameover();
		}
	}

	public void gameover(){
		transform.gameObject.GetComponent<Animation> ().Play ();
		GameObject.Find ("Canvas").GetComponent<Animation> ().Play ();
		this.tag = "Untagged";
		GameObject.Find ("highscore").GetComponent<Text> ().text = "Best: " + kills;

		if (PlayerPrefs.GetInt ("High Score") != 0) {

			if(kills > PlayerPrefs.GetInt ("High Score")){
				PlayerPrefs.SetInt ("High Score", kills);
			}
		} else {
			PlayerPrefs.SetInt("High Score", kills);
		}
		fadeout = true;
	}

	public void killCounter(int points){
		kills += points;
		GameObject.FindGameObjectWithTag ("Score").GetComponent<Text> ().text = "Kills: "+kills;

		if (kills > 29) {
			GameObject.Find("Kills").GetComponent<Text>().color = Color.red;

			if(kills > PlayerPrefs.GetInt("High Score")){
				GameObject.Find("Kills").GetComponent<Text>().color = Color.yellow;
			}
		}
	}
}
