﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class loadHighscore : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.Find("highscore").GetComponent<Text>().text = "Best: "+PlayerPrefs.GetInt("High Score");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
